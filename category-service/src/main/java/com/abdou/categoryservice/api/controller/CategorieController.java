package com.abdou.categoryservice.api.controller;

import com.abdou.categoryservice.api.entity.Categorie;
import com.abdou.categoryservice.api.service.categorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/categorie")
public class CategorieController {
    @Autowired
    private categorieService categorieService;
    @PostMapping("/addCategorie")
    public Categorie save(@RequestBody Categorie categorie) {
        return categorieService.saveCategorie(categorie);
    }
    @RequestMapping("/getOneCategorie/{libelle}")
    public Categorie getByLibelle(@PathVariable String libelle) {
        return categorieService.findByLibelle(libelle);
    }
}
