package com.abdou.gatewayeventpush;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HystrixFallbackController {
    @RequestMapping("/categorieFallback")
    public Mono<String> categorieServiceFallback(){
        return Mono.just("Le service catégorie prend du temps à répondre. Veuillez réessayer ultérieurement.");
    }
    @RequestMapping("/eventFallback")
    public Mono<String> evenementServiceFallback(){
        return Mono.just("Le service évènement prend du temps à répondre. Veuillez réessayer ultérieurement.");
    }
}
