package com.abdou.eventservice.api.controller;

import com.abdou.eventservice.api.entity.Evenement;
import com.abdou.eventservice.api.service.evenementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
public class evenementController {
    @Autowired
    private evenementService evenementService;
    @PostMapping("/addEvent")
    public Evenement save(@RequestBody Evenement evenement) {
        return evenementService.saveEvenement(evenement);
    }
}
