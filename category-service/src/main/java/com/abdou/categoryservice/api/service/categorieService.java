package com.abdou.categoryservice.api.service;

import com.abdou.categoryservice.api.entity.Categorie;
import com.abdou.categoryservice.api.repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class categorieService {
    @Autowired
    private CategorieRepository categorieRepository;
    public Categorie saveCategorie(Categorie categorie){
        return categorieRepository.save(categorie);
    }
    public Categorie findByLibelle(String libelle){
        return categorieRepository.findByLibelle(libelle);
    }
}
